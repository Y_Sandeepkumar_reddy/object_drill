function keys (obj) {
  if (!typeof obj === 'object' || obj === null) {
    return null ;
  }

  let output = [] ;
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      output.push(String(key)) ;
    }
  }
  return output;
}

module.exports = keys;
