function invert (obj) {
  if (!(typeof obj === 'object') || obj === null) {
    return typeof obj ;
  }

  let output = {} ;
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      output[String(obj[key])] = key ;
    }
  }

  return output ;
}

module.exports = invert ;
